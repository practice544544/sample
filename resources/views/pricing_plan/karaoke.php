<?php
$cookie_language = "language";

$default_locale = "en-us";
$default_language = "English (US)";
$current_locale = $default_locale;
$current_language = $default_language;

$locale_array = ["en-us", "zh-cn"];

$languages = array(
    "en-us" => "English (US)",
    "zh-cn" => "中文（简体）"
);

if (!isset($_COOKIE[$cookie_language])) {
    setcookie($cookie_language, $default_locale, time() + (86400 * 30), "/");
} else if (!in_array($_COOKIE[$cookie_language], $locale_array)) {
    setcookie($cookie_language, $default_locale, time() + (86400 * 30), "/");
} else {
    $current_locale = $_COOKIE[$cookie_language];
}

$file_name = "../locale/$current_locale.json";
$current_language = $languages[$current_locale];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Eastgate.Tech | Plans</title>
    <link rel="icon" href="/favicon.png?v=2" type="image/x-icon" />
    <link href="../css/sections/pricing_plan.css?dummy=8484746" rel="stylesheet" type="text/css" />
    <link href="../css/master.css" rel="stylesheet" type="text/css" />
    <!-- FontsAwesome -->
    <link href="../css/fontawesome/all.css?1604583668" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,400;0,500;0,600;1,600&display=swap"
        rel="stylesheet">
    <link href="../css/sections/footer.css?1604583663" rel="stylesheet" type="text/css" />

    <!-- Scripting Libraries -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/swiper.min.js"></script>

    <!-- Locale -->
    <script>
    var jsonData = <?php include($file_name); ?>;
    </script>
</head>

<body>

    <div class="app__container">
        <header class="header__container">
            <nav class="navbar">
                <div class=navbar__relative__container>
                    <ul class='nav_menu'>
                        <li class="logo__container">
                            <a href="/" class="navbar_logo">
                                <img src="../assets/images/logo.png" alt="logo mobile" width="200" />
                            </a>
                        </li>

                        <li class="nav_item">
                            <a href="/#about" class="nav_links translatables" data-translate="nav_about_us">About Us</a>
                        </li>

                        <!-- <li class="nav_item">
                            <a href="#portfolio" class="nav_links translatables" data-translate="nav_portfolio">Portfolio</a>
                        </li> -->

                        <li class="nav_item">
                            <a href="/#product_technology" class="nav_links translatables"
                                data-translate="nav_technologies">Technologies</a>
                        </li>

                        <li class="nav_item">
                            <a href="/#testimonial" class="nav_links translatables"
                                data-translate="nav_testimonials">Testimonials</a>
                        </li>

                        <!-- <li class="nav_item flex_center">
                            <a href="#jobs" class="nav_links translatables" data-translate="nav_jobs">Jobs</a>
                        </li> -->

                        <li class="nav_item">
                            <a href="/#team" class="nav_links translatables" data-translate="nav_team">Team</a>
                        </li>

                        <li class="nav_item dropdown" id="dropdown__language">
                            <!-- <div class="dropdown__container">
                                <div class="dropdown">
                                    <a href="#" class="nav_links" id="language">
                                        <?php echo $current_language; ?>
                                    </a>
                                    <div class="dropdown-content">
                                        <a href="#" class="language_switch" data-lang="en-us">English (US)</a>
                                        <a href="#" class="language_switch" data-lang="zh-cn">中文（简体）</a>
                                    </div>
                                </div>
                                <i class="fas fa-caret-down icon__dropdown"></i>
                            </div> -->
                            <label class="dropdown">
                                <div class="dd-button">
                                    <?php echo $current_language; ?>
                                </div>
                                <input type="checkbox" class="dd-input" id="test">

                                <ul class="dd-menu">
                                    <li class="language_switch" data-lang="en-us">English</li>
                                    <li class="divider"></li>
                                    <li class="language_switch" data-lang="zh-cn">中文（简体）</li>

                                </ul>
                            </label>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <section class="section__pricing width_1366 grid__two">
            <div class="solution__image" id="real__estate__image">
                <img src="../assets/images/technology/karaoke_solution2.png?1604583661" alt="solution" />
            </div>
            <div class="pricing__plan__details">
                <h1 class="solution__title translatables" data-translate="karaoke_technology_title"
                    id="real__estate__title">Real Estate Solution</h1>
                <span class="solution__description translatables" data-translate="karaoke_solution_description">
                    Song chips are a thing of the past, with MUSIKAT.TV, you just need the web browser from your smart
                    tv, game consoles, tablets, smartphones or computers to enjoy hundreds of opm songs.
                </span>
                <div class="contact__sales">
                    <span class="contact__description translatables" data-translate="contact_details">For more details
                        contact us via</span>
                    <div class="dp__flex"><a href="mailto:contact@eastgate.tech"><img
                                src="/assets/images/contact__png.png" / alt="footer contact"></a></div>
                    <!-- <span class="contact__button translatables" data-translate="contact_sales">Contact Sales</span> -->
                </div>
            </div>


        </section>
        <!-- <div class="contact__container width_1366">
            <div class="contact__sales">
                <span class="contact__description translatables" data-translate="contact_details">For more details just contact us.</span>
                <span class="contact__button translatables" data-translate="contact_sales">Contact Sales</span>
            </div>
        </div> -->

        <div class="section__footer">
            <div class="footer__container width_1366">
                <div class="footer__logo">
                    <img src="/assets/images/footer-logo.png" alt="footer logo" />
                </div>
                <div class="footer__navigation">
                    <span class="footer__title translatables" data-translate="navigation">Navigation</span>
                    <ul class="footer__list">
                        <li><a class="text_decoration_none translatables" data-translate="nav_about_us"
                                href="/#about">About Us</a></li>
                        <!-- <li><a class="text_decoration_none translatables" data-translate="nav_portfolio" href="#portfolio">Portfolio</a></li> -->
                        <li><a class="text_decoration_none translatables" data-translate="nav_technologies"
                                href="/#product_technology">Technologies</a></li>
                        <li><a class="text_decoration_none translatables" data-translate="nav_testimonials"
                                href="/#testimonial">Testimonials</a></li>
                        <!-- <li><a class="text_decoration_none translatables" data-translate="nav_jobs" href="#jobs">Jobs</a></li> -->
                        <li><a class="text_decoration_none translatables" data-translate="nav_team"
                                href="/#team">Team</a></li>
                    </ul>
                </div>
                <div class="footer__contact">
                    <span class="footer__title translatables" data-translate="contact_us">Contact Us</span>

                    <ul class="footer__list">
                        <li><span><i class="fas fa-map-marker-alt"></i> Malolos City, Philippines 3000</span></li>

                        <!-- <li class="contact"><span><i class="far fa-envelope"></i> contact@eastgate.tech</span></li> -->
                        <li class="contact">
                            <div class="footer__contact"><img src="/assets/images/footer-contact.png" /
                                    alt="footer contact"></div>
                        </li>

                    </ul>

                    <div class="footer__social__media">
                        <a href="https://www.facebook.com/eastgate.tech" class="footer__link"><i
                                class="fab fa-facebook"></i></a>
                        <!-- <a href="https://twitter.com/?lang=en" class="footer__link"><i class="fab fa-twitter"></i></a> -->
                        <a href="https://www.instagram.com/eastgate.tech/?hl=af" class="footer__link"><i
                                class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- LOCALIZATION -->
    <script>
    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " ") c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    $(".translatables").each((index, elem) => {
        const translate_key = elem.getAttribute("data-translate");
        const translate_value = jsonData[translate_key];
        $(elem).text(translate_value);
    });

    $(".language_switch").click(function() {
        const language = $(this).data("lang");
        setCookie("language", language, 30);
        window.location.reload();
    });

    $("body").css({
        visibility: "visible"
    });
    </script>
</body>

</html>