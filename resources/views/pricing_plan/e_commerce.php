<?php
$cookie_language = "language";

$default_locale = "en-us";
$default_language = "English (US)";
$current_locale = $default_locale;
$current_language = $default_language;

$locale_array = ["en-us", "zh-cn"];

$languages = array(
    "en-us" => "English (US)",
    "zh-cn" => "中文（简体）"
);

if (!isset($_COOKIE[$cookie_language])) {
    setcookie($cookie_language, $default_locale, time() + (86400 * 30), "/");
} else if (!in_array($_COOKIE[$cookie_language], $locale_array)) {
    setcookie($cookie_language, $default_locale, time() + (86400 * 30), "/");
} else {
    $current_locale = $_COOKIE[$cookie_language];
}

$file_name = "../locale/$current_locale.json";
$current_language = $languages[$current_locale];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Eastgate.Tech | Plans</title>
    <link rel="icon" href="/favicon.png?v=2" type="image/x-icon" />
    <link href="../css/sections/pricing_plan.css?dummy=8484747" rel="stylesheet" type="text/css" />
    <link href="../css/master.css" rel="stylesheet" type="text/css" />
    <!-- FontsAwesome -->
    <link href="../css/fontawesome/all.css?1604583667" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,400;0,500;0,600;1,600&display=swap"
        rel="stylesheet">
    <link href="../css/sections/footer.css?1604583663" rel="stylesheet" type="text/css" />

    <!-- Scripting Libraries -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/swiper.min.js"></script>

    <!-- Locale -->
    <script>
    var jsonData = <?php include($file_name); ?>;
    </script>
</head>

<body>
    <div class="app__container">
        <header class="header__container">
            <nav class="navbar">
                <div class=navbar__relative__container>
                    <ul class='nav_menu'>
                        <li class="logo__container">
                            <a href="/" class="navbar_logo">
                                <img src="../assets/images/logo.png" alt="logo mobile" width="200" />
                            </a>
                        </li>

                        <li class="nav_item">
                            <a href="/#about" class="nav_links translatables" data-translate="nav_about_us">About Us</a>
                        </li>

                        <!-- <li class="nav_item">
                            <a href="#portfolio" class="nav_links translatables" data-translate="nav_portfolio">Portfolio</a>
                        </li> -->

                        <li class="nav_item">
                            <a href="/#product_technology" class="nav_links translatables"
                                data-translate="nav_technologies">Technologies</a>
                        </li>

                        <li class="nav_item">
                            <a href="/#testimonial" class="nav_links translatables"
                                data-translate="nav_testimonials">Testimonials</a>
                        </li>

                        <!-- <li class="nav_item flex_center">
                            <a href="#jobs" class="nav_links translatables" data-translate="nav_jobs">Jobs</a>
                        </li> -->

                        <li class="nav_item">
                            <a href="/#team" class="nav_links translatables" data-translate="nav_team">Team</a>
                        </li>

                        <li class="nav_item dropdown" id="dropdown__language">
                            <!-- <div class="dropdown__container">
                                <div class="dropdown">
                                    <a href="#" class="nav_links" id="language">
                                        <?php echo $current_language; ?>
                                    </a>
                                    <div class="dropdown-content">
                                        <a href="#" class="language_switch" data-lang="en-us">English (US)</a>
                                        <a href="#" class="language_switch" data-lang="zh-cn">中文（简体）</a>
                                    </div>
                                </div>
                                <i class="fas fa-caret-down icon__dropdown"></i>
                            </div> -->
                            <label class="dropdown">
                                <div class="dd-button">
                                    <?php echo $current_language; ?>
                                </div>
                                <input type="checkbox" class="dd-input" id="test">

                                <ul class="dd-menu">
                                    <li class="language_switch" data-lang="en-us">English</li>
                                    <li class="divider"></li>
                                    <li class="language_switch" data-lang="zh-cn">中文（简体）</li>

                                </ul>
                            </label>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <section class="section__pricing width_1366 grid__two">
            <div class="solution__image">
                <img src="../assets/images/technology/chicken_tool3.png" alt="solution" />
            </div>
            <div class="pricing__plan__details">
                <span class="solution__title translatables" data-translate="ecommerce_technology_title">E-Commerce
                    Solution</span>
                <span class="solution__description translatables" data-translate="ecommerce_solution_description">
                    This Web Application can help increase your sales and manage your products. Automate and view
                    details
                    listing.
                    We can also customize your front store website using content manager settings. Change the color
                    dynamically
                    and change logo. This Web Application also have a mobile application that users can download and
                    access
                    in
                    appstore or playstore.
                </span>
            </div>
        </section>
        <div class="pricing__background">
            <div class="pricing__plan width_1366">
                <div class="card">
                    <span class="card__title translatables" data-translate="basic_plan">
                        Basic Plan
                    </span>
                    <span class="starts__at translatables" data-translate="starts_at">Starts at</span>
                    <span class="card__price"><span id="peso_sign">₱</span>499</span>
                    <span class="card__date">/ <span class="translatables"
                            data-translate="month_abev">month</span></span>
                    <ul class="card__listings">
                        <li class="feature bold">
                            <span class="translatables" data-translate="branches">Branches</span>:
                            <span> 1</span>
                            <span class="translatables" data-translate="branch"> Branch</span>
                        </li>
                        <li class="feature bold">
                            <span class="translatables" data-translate="site_managers">Site Managers</span>:
                            <span> 1</span>
                            <span class="translatables" data-translate="site_manager_account"> Manager Account</span>
                        </li>
                        <li class="divider__pricing"></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_food_menu_pricing">Food menu and pricing management</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_online_ordering">Online ordering and order management</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_sms_verify_order">SMS-verified orders</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_cod_checkout">Cash on delivery chekcout</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_content_management">Content management</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_seo">Mobile, social media and SEO friendly website</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_business_inquiry">Business inquiry form and data management</span>
                        </li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="locationlization">Locationlization</span></li>
                    </ul>
                </div>
                <div class="card active">
                    <span class="card__title translatables" data-translate="essential_plan">
                        Essential Plan
                    </span>
                    <span class="starts__at translatables" data-translate="starts_at">Starts at</span>
                    <span class="card__price"><span id="peso_sign">₱</span>1,499</span>
                    <span class="card__date">/ <span class="translatables"
                            data-translate="month_abev">month</span></span>
                    <ul class="card__listings">
                        <li class="feature bold">
                            <span class="translatables" data-translate="branches">Branches</span>:
                            <span> 5</span>
                            <span class="translatables" data-translate="branches"> Branches</span>
                        </li>
                        <li class="feature bold">
                            <span class="translatables" data-translate="site_managers">Site Managers</span>:
                            <span> 10</span>
                            <span class="translatables" data-translate="site_manager_account"> Manager Accounts</span>
                        </li>
                        <li class="divider__pricing"></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_basic_plus">Basic package, plus</span>:</li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_branch_manager">Branch-level site manager access and online order
                                management</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_mobile_apps">Mobile apps (both Apple and Google app)</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_order_history">Customer order history</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_loyalty">Loyalty rewarads</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_discount">Discount promos</span></li>
                    </ul>
                </div>
                <div class="card">
                    <span class="card__title translatables" data-translate="premium_plan">
                        Premium Plan
                    </span>
                    <div class="card__contact__container">
                        <span class="card__contact translatables" data-translate="contact_sales">Contact Sales</span>
                    </div>
                    <ul class="card__listings">
                        <li class="feature bold">
                            <span class="translatables" data-translate="branches">Branches</span>:
                            <span> 1000+</span>
                        </li>
                        <li class="feature bold">
                            <span class="translatables" data-translate="site_managers">Site Managers</span>:
                            <span> 1000+</span>
                        </li>
                        <li class="divider__pricing"></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_essential_plus">Essential package, plus:</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_payment_management">Online payments order management</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_delivery_management">Food delivery management</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_analytics">Customer analytics</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_custom_mobile">Custom website and app</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_enterprise">Enterprise integration</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_site_reliability">Increased site reliability</span></li>
                        <li class="feature"><i class="fas fa-check"></i> <span class="translatables"
                                data-translate="plan_internationalization">Internationalization</span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section__footer">
            <div class="footer__container width_1366">
                <div class="footer__logo">
                    <img src="/assets/images/footer-logo.png" alt="footer logo" />
                </div>
                <div class="footer__navigation">
                    <span class="footer__title translatables" data-translate="navigation">Navigation</span>
                    <ul class="footer__list">
                        <li><a class="text_decoration_none translatables" data-translate="nav_about_us"
                                href="/#about">About Us</a></li>
                        <!-- <li><a class="text_decoration_none translatables" data-translate="nav_portfolio" href="#portfolio">Portfolio</a></li> -->
                        <li><a class="text_decoration_none translatables" data-translate="nav_technologies"
                                href="/#product_technology">Technologies</a></li>
                        <li><a class="text_decoration_none translatables" data-translate="nav_testimonials"
                                href="/#testimonial">Testimonials</a></li>
                        <!-- <li><a class="text_decoration_none translatables" data-translate="nav_jobs" href="#jobs">Jobs</a></li> -->
                        <li><a class="text_decoration_none translatables" data-translate="nav_team"
                                href="/#team">Team</a></li>
                    </ul>
                </div>
                <div class="footer__contact">
                    <span class="footer__title translatables" data-translate="contact_us">Contact Us</span>

                    <ul class="footer__list">
                        <li><span><i class="fas fa-map-marker-alt"></i> Malolos City, Philippines 3000</span></li>

                        <!-- <li class="contact"><span><i class="far fa-envelope"></i> contact@eastgate.tech</span></li> -->
                        <li class="contact">
                            <div class="footer__contact"><img src="/assets/images/footer-contact.png" /
                                    alt="footer contact"></div>
                        </li>

                    </ul>

                    <div class="footer__social__media">
                        <a href="https://www.facebook.com/eastgate.tech" class="footer__link"><i
                                class="fab fa-facebook"></i></a>
                        <!-- <a href="https://twitter.com/?lang=en" class="footer__link"><i class="fab fa-twitter"></i></a> -->
                        <a href="https://www.instagram.com/eastgate.tech/?hl=af" class="footer__link"><i
                                class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- LOCALIZATION -->
    <script>
    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " ") c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    $(".translatables").each((index, elem) => {
        const translate_key = elem.getAttribute("data-translate");
        const translate_value = jsonData[translate_key];
        $(elem).text(translate_value);
    });

    $(".language_switch").click(function() {
        const language = $(this).data("lang");
        setCookie("language", language, 30);
        window.location.reload();
    });

    $("body").css({
        visibility: "visible"
    });
    </script>
</body>

</html>