<?php
$cookie_language = "language";

$default_locale = "en-us";
$default_language = "English (US)";
$current_locale = $default_locale;
$current_language = $default_language;

$locale_array = ["en-us", "zh-cn"];

$languages = array(
    "en-us" => "English (US)",
    "zh-cn" => "中文（简体）"
);

if (!isset($_COOKIE[$cookie_language])) {
    setcookie($cookie_language, $default_locale, time() + (86400 * 30), "/");
} else if (!in_array($_COOKIE[$cookie_language], $locale_array)) {
    setcookie($cookie_language, $default_locale, time() + (86400 * 30), "/");
} else {
    $current_locale = $_COOKIE[$cookie_language];
}

$file_name = "locale/$current_locale.json";
$current_language = $languages[$current_locale];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

    <title>Eastgate.Tech</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,400;0,500;0,600;1,600&display=swap"
        rel="stylesheet">

    <link rel="icon" href="favicon.png?v=2" type="image/x-icon" />

    <!-- FontsAwesome -->
    <link href="css/fontawesome/all.css?1604583668" rel="stylesheet">
    <!-- Master -->
    <link href="css/master.css?1604583661" rel="stylesheet" type="text/css" />
    <!-- Master -->
    <link href="css/navigation/navigation.css?1604583668" rel="stylesheet" type="text/css" />

    <!-- Sections -->
    
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/sections.css') }}?{{rand()}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/portfolio.css') }}?{{rand()}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/about.css') }}?{{rand()}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/testimonial.css') }}?{{rand()}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/technology.css') }}?{{rand()}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/jobs.css') }}?{{rand()}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/location.css') }}?{{rand()}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/team.css') }}?{{rand()}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/product_technology.css') }}?{{rand()}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/footer.css') }}?{{rand()}}"/>
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/sections/mobile.css') }}?{{rand()}}"/> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/swiper.min.css') }}?{{rand()}}"/>

    <!-- Locale -->
    <script>
    var jsonData = <?php include($file_name); ?>;
    </script>
</head>

<body>
    <div class="app__container">
        <div class="header">
            <header class="header__container">
                <nav class="navbar">
                    <div class=navbar__relative__container>
                        <ul class='nav_menu'>
                            <li class="logo__container">
                                <a href="/" class="navbar_logo">
                                    <img src="assets/images/logo.png" alt="logo mobile" width="200" />
                                </a>
                            </li>

                            <li class="nav_item">
                                <a href="#about" class="nav_links translatables" data-translate="nav_about_us">About
                                    Us</a>
                            </li>

                            <!-- <li class="nav_item">
                                <a href="#portfolio" class="nav_links translatables" data-translate="nav_portfolio">Portfolio</a>
                            </li> -->

                            <li class="nav_item">
                                <a href="#product_technology" class="nav_links translatables"
                                    data-translate="nav_technologies">Technologies</a>
                            </li>

                            <li class="nav_item">
                                <a href="#testimonial" class="nav_links translatables"
                                    data-translate="nav_testimonials">Testimonials</a>
                            </li>

                            <!-- <li class="nav_item flex_center">
                                <a href="#jobs" class="nav_links translatables" data-translate="nav_jobs">Jobs</a>
                            </li> -->

                            <li class="nav_item">
                                <a href="#team" class="nav_links translatables" data-translate="nav_team">Team</a>
                            </li>

                            <li class="nav_item dropdown" id="dropdown__language">
                                <!-- <div class="dropdown__container">
                                    <div class="dropdown">
                                        <a href="#" class="nav_links" id="language">
                                            <?php echo $current_language; ?>
                                        </a>
                                        <div class="dropdown-content">
                                            <a href="#" class="language_switch" data-lang="en-us">English (US)</a>
                                            <a href="#" class="language_switch" data-lang="zh-cn">中文（简体）</a>
                                        </div>
                                    </div>
                                    <i class="fas fa-caret-down icon__dropdown"></i>
                                </div> -->
                                <label class="dropdown">
                                    <div class="dd-button">
                                        <?php echo $current_language; ?>
                                    </div>
                                    <input type="checkbox" class="dd-input" id="test">

                                    <ul class="dd-menu">
                                        <li class="language_switch" data-lang="en-us">English</li>
                                        <li class="divider"></li>
                                        <li class="language_switch" data-lang="zh-cn">中文（简体）</li>

                                    </ul>
                                </label>
                            </li>
                        </ul>
                    </div>
                </nav>
                <a href="#portfolio" class="scroll_a"><span></span>See More</a>
            </header>
        </div>

        <div class="section__container">
            <div class="section__about" id="about">
                <div class="about__container">
                    <div class="about__image">
                        <img src="assets/images/section/aboutman.png?dummy=8484748" alt="about" />
                    </div>
                    <div class="about__text__container" style="min-height: 294px;">
                        <span class="about__title translatables" data-translate="feature_about_us">About Us</span>
                        <span class="about__description translatables" data-translate="about_us_section_text">We focus
                            on building Software-as-a-Service products that are
                            aligned with the digitization of various industries. The company is founded by a team of
                            veteran engineers and inventors who worked and were recognized at numerous global companies
                            in the field of software, data intelligence, smartphone, and telecommunications
                            engineering.</span>
                    </div>
                </div>
            </div>

            <div class="section__portfolio" id="portfolio">
                <span class="section__title translatables" data-translate="feature_portfolio"> Our Portfolio</span>
                <!-- Swiper -->
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg"
                                    style="background-image: url(assets/images/portfolio/phrentals4.png?dummy=8484746)">
                                    <a class="location" href="https://ph.rentals/" target="_blank">PH.Rentals</a>
                                </div>
                            </div>
                        </div>


                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg no_cursor"
                                    style="background-image: url(assets/images/portfolio/kendura.png?dummy=8484745);">
                                    <!-- <a class="location" href="https://www.kendura.net/" target="_blank">Kendura</a> -->
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg"
                                    style="background-image: url(assets/images/portfolio/phsale3.png?dummy=8484745);">
                                    <a class="location" href="https://ph.sale/" target="_blank">PH.Sale</a>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg"
                                    style="background-image: url(assets/images/portfolio/phhouse2.png?dummy=8484745);">
                                    <a class="location" href="https://ph.house/" target="_blank">PH.House</a>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg no_cursor"
                                    style="background-image: url(assets/images/portfolio/jacksgrill.png?dummy=8484744);">
                                    <!-- <a class="location" href="http://jacksgrill.demoz.live/" target="_blank">Jacks
                                        Grill</a> -->
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg"
                                    style="background-image: url(assets/images/portfolio/phcondo2.png?dummy=8484744);">
                                    <a class="location" href="http://ph.condos/" target="_blank">PH.Condos</a>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg"
                                    style="background-image: url(assets/images/portfolio/panpacificholdings.png?dummy=8484742)">
                                    <a class="location" href="https://www.panpacificholdings.org/" target="_blank">Pan
                                        Pacific
                                        Holdings</a>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg"
                                    style="background-image: url(assets/images/portfolio/phvacation2.png?dummy=8484744);">
                                    <a class="location" href="http://ph.vacations/" target="_blank">PH.Vacations</a>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg no_cursor"
                                    style="background-image: url(assets/images/portfolio/groceries3.png?dummy=8484742)">

                                </div>
                            </div>
                        </div>
                        <!-- <div class=" swiper-slide portfolio-swiper">
                                        <div class="card">
                                            <div class="card-bg no_cursor"
                                                style="background-image: url(assets/images/portfolio/4wclean.png?dummy=8484741)">
                                            </div>
                                        </div>
                                </div> -->
                        <!-- <div class="swiper-slide">
                            <div class="card">
                                <div class="card-bg"
                                    style="background-image: url(assets/images/portfolio/vape.jpg?dummy=8484744)">
                                    <a class="location" href="#" target="_blank">Vape</a>
                                </div>
                            </div>
                        </div> -->
                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg no_cursor"
                                    style="background-image: url(assets/images/portfolio/handsnmind.png?dummy=8484711);">
                                    <!-- <a class="location" href="">Hands N
                                        Mind</a> -->
                                </div>
                            </div>
                        </div>


                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg"
                                    style="background-image: url(assets/images/portfolio/perabiz.png?dummy=8484711);">
                                    <a class="location" href="https://pera.biz/" target="_blank">Pera.Biz</a>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide portfolio-swiper">
                            <div class="card">
                                <div class="card-bg no_cursor"
                                    style="background-image: url(assets/images/portfolio/musikat.png?dummy=8484745);">
                                    <!-- <a class="location" href="https://www.musikat.tv/" target="_blank">Musikat.TV</a> -->
                                </div>
                            </div>
                        </div>

                        <!-- <div class="swiper-slide">
                            <div class="card">
                                <div class="card-bg"
                                    style="background-image: url(assets/images/portfolio/ph_singles.png?dummy=8484711);">
                                    <a class="location" href="https://ph.singles/" target="_blank">PH.Singles</a>
                                </div>
                            </div>
                        </div> -->

                    </div>
                </div>
                <!-- <div class="slideshow__container">
                    <div class="overflow">
                        <div class="carousel">
                            <div class="slider">

                                <div class="card">
                                    <div class="card-bg"
                                        style="background-image: url(assets/images/portfolio/phrentals.png)">
                                        <a class="location" href="https://ph.rentals/" target="_blank">PH.Rentals</a>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-bg"
                                        style="background-image: url(assets/images/portfolio/musikat.jpg);">
                                        <a class="location" href="https://www.musikat.tv/"
                                            target="_blank">Musikat.TV</a>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-bg"
                                        style="background-image: url(assets/images/portfolio/jacksgrill.jpg);">
                                        <a class="location" href="http://jacksgrill.demoz.live/menus"
                                            target="_blank">Jacks Grill</a>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-bg"
                                        style="background-image: url(assets/images/portfolio/panpacificholdings.jpg)">
                                        <a class="location" href="https://www.panpacificholdings.org/"
                                            target="_blank">Pan Pacific
                                            Holdings</a>
                                    </div>
                                </div>


                                <div class="card">
                                    <div class="card-bg"
                                        style="background-image: url(assets/images/portfolio/4wclean.jpg)">
                                        <a class="location" href="http://4wclean.com/" target="_blank">4wclean</a>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-bg"
                                        style="background-image: url(assets/images/portfolio/vape.jpg)">
                                        <a class="location" href="#" target="_blank">Vape</a>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-bg"
                                        style="background-image: url(assets/images/portfolio/handsnmind.jpg);">
                                        <a class="location" href="http://event.handsnmind.com/event"
                                            target="_blank">Hands N Mind</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>


            <div class="section__product__technology" id="product_technology">
                <span class="section__title translatables" data-translate="feature_technologies">Our
                    Technologies</span>


                <a href="/pricing_plan/real_estate">
                    <div class="product__technology__container">
                        <div class="product__technology__image">
                            <img src="assets/images/technology/phrental_tool_5.png?dummy=8484745"
                                alt="Ph Rentals Technology" />
                        </div>

                        <div class="product__technology__text">
                            <span class="product__technology__title translatables"
                                data-translate="estate_technology_title">Real Estate Solution</span>
                            <span class="product__technology__description translatables"
                                data-translate="estate_technology_description">
                                Worry No More! Make your rental business easy to find on the internet with our
                                search
                                engine optimization (SEO).
                            </span>
                        </div>
                    </div>
                </a>

                <a href="/pricing_plan/buy_and_sell">
                    <div class="product__technology__container">
                        <div class="product__technology__image">
                            <img src="assets/images/technology/phsale_tool_4.png?dummy=8484745"
                                alt="Ph Rentals Technology" />
                        </div>

                        <div class="product__technology__text">
                            <span class="product__technology__title translatables"
                                data-translate="buy_sell_technology_title">Buy And Sell Site</span>
                            <span class="product__technology__description translatables"
                                data-translate="buy_sell_technology_description">
                                Where you can post or find all the best deals in the Philippines.
                            </span>
                        </div>
                    </div>
                </a>


                <div class="product__technology__container">
                    <div class="product__technology__image">
                        <img src="assets/images/technology/chicken_tool3.png?dummy=8484714"
                            alt="E Commerce Technology" />
                    </div>
                    <div class="product__technology__text">
                        <span class="product__technology__title translatables"
                            data-translate="ecommerce_technology_title">E-Commerce Solution</span>
                        <span class="product__technology__description translatables"
                            data-translate="ecommerce_technology_description">
                            This solution is a great help for your business. Easy to use and client
                            transactions
                            will become faster.
                        </span>
                    </div>
                </div>



                <a href="https://pera.biz/?sort=date_descending">
                    <div class="product__technology__container">
                        <div class="product__technology__image">
                            <img src="assets/images/technology/perabiz_tool2.png?dummy=8484741" alt="Jobs Technology" />
                        </div>
                        <div class="product__technology__text">
                            <span class="product__technology__title translatables"
                                data-translate="jobs_technology_title">Jobs Site</span>
                            <span class="product__technology__description translatables"
                                data-translate="jobs_technology_description">
                                Find and Apply Jobs in the Philippines
                            </span>
                        </div>
                    </div>
                </a>


                <div class="product__technology__container">
                    <div class="product__technology__image">
                        <img src="assets/images/technology/musikat_tool2.jpg?dummy=8484741" alt="Musikat Technology" />
                    </div>
                    <div class="product__technology__text">
                        <span class="product__technology__title translatables"
                            data-translate="karaoke_technology_title">Karaoke Streaming Solution</span>
                        <span class="product__technology__description translatables"
                            data-translate="karaoke_technology_description">
                            Enjoy bonding with your family with this solution. You just need the web browser
                            from
                            your smart tv, game consoles, tablets, smartphones or computers to enjoy
                            hundreds of opm
                            songs.
                        </span>
                    </div>
                </div>


                <a href="http://developers.continue.mobi/">
                    <div class="product__technology__container">
                        <div class="product__technology__image">
                            <img src="assets/images/technology/continue_mobi_tool.jpg?dummy=8484741"
                                alt="Mobile Technology" />
                        </div>
                        <div class="product__technology__text">
                            <span class="product__technology__title translatables"
                                data-translate="mobile_technology_title">Continue Mobile</span>
                            <span class="product__technology__description translatables"
                                data-translate="mobile_technology_description">
                                Easy to use using just your phone. Login anytime and anywhere you want. Hassle
                                free
                                password using OTP with continue.mobi
                            </span>
                        </div>
                    </div>
                </a>


                <!-- <a href="https://ph.singles/">
                    <div class="product__technology__container">
                        <div class="product__technology__image">
                            <img src="assets/images/technology/dating_site_tool.jpg?dummy=8484741"
                                alt="Dating Technology" />
                        </div>
                        <div class="product__technology__text">
                            <span class="product__technology__title translatables"
                                data-translate="dating_technology_title">Dating Site</span>
                            <span class="product__technology__description translatables"
                                data-translate="dating_technology_description">
                                Singles are more fun in the Philippines
                            </span>
                        </div>
                    </div>
                </a> -->

            </div>

            <div class="section__testimonial" id="testimonial">
                <span class="section__title translatables" data-translate="feature_testimonials">Client
                    Testimonials</span>
                <div class="testimonial__container">
                    <div class="testimonial__card">
                        <div class="testimonial__card__description">
                            <i class="fas fa-quote-left testimonial__icon"></i>
                            <span class="testimonial__card__text">
                                "..The team is talented with web and mobile application development, and they
                                deliver
                                projects on time"
                            </span>
                            <div class="testimonial__card__signature">
                                <div class="testimonial__card__dash"></div>
                                <span class="testimonial__card__name">Jef</span>
                                <span class="testimonial__card__position">Director Software Engineering</span>
                                <span class="testimonial__card__location">Seattle Washington, USA</span>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial__card">
                        <div class="testimonial__card__description">
                            <i class="fas fa-quote-left testimonial__icon"></i>
                            <span class="testimonial__card__text">
                                "...The team is very professional, helpful and courteous in every meeting...
                                they make
                                us feel comfortable by being respectful and flexible with our ideas. They
                                completed the
                                project on time and the website helped in launching our company!"
                            </span>
                            <div class="testimonial__card__signature">
                                <div class="testimonial__card__dash"></div>
                                <span class="testimonial__card__name">Ria</span>
                                <span class="testimonial__card__position">Business Owner</span>
                                <span class="testimonial__card__location">Perth, Australia</span>
                            </div>
                        </div>
                    </div>
                    <div></div>

                </div>
            </div>

            <div class="section__location">
                <div class="location__container">
                    <div class="location__image">
                        <img src="assets/images/section/location.png?dummy=8484749" alt="about" />
                    </div>
                    <div class="location__text__container">
                        <div class="location__text">
                            <span class="location__title translatables" data-translate="feature_team">Meet The
                                Team</span>
                        </div>
                    </div>
                </div>
            </div>
            <section class="section__team" id="team">
                <div class="team__slider" id="main-slider">
                    <!-- outermost container element -->
                    <div class="team__slider-wrapper">
                        <!-- innermost wrapper element -->
                        <img src="assets/images/carousel/carousel-12.jpg?dummy=8484742" alt="Twelve"
                            class="team__slide" />
                        <img src="assets/images/carousel/carousel-08.jpg?dummy=8484742" alt="Zero"
                            class="team__slide" />
                        <img src="assets/images/carousel/carousel-11.jpg?dummy=8484742" alt="Ten" class="team__slide" />
                        <img src="assets/images/carousel/carousel-09.jpg?dummy=8484742" alt="Eight"
                            class="team__slide" />
                        <img src="assets/images/carousel/carousel-01.jpg?dummy=8484741" alt="First"
                            class="team__slide" />
                        <img src="assets/images/carousel/carousel-02.jpg?dummy=8484746" alt="Second"
                            class="team__slide" />
                        <img src="assets/images/carousel/carousel-03.jpg?dummy=8484744" alt="Third"
                            class="team__slide" />
                        <!-- <img src="assets/images/carousel/slideshow4.png?dummy=8484741" alt="Fourth" class="team__slide" />
                        <img src="assets/images/carousel/slideshow5.png?dummy=8484742" alt="Fifth" class="team__slide" /> -->
                        <img src="assets/images/carousel/carousel-06.jpg?dummy=8484747" alt="Six" class="team__slide" />
                        <img src="assets/images/carousel/carousel-07.jpg?dummy=8484748" alt="Seven"
                            class="team__slide" />



                    </div>
                </div>
            </section>


            <div class="section__jobs" id="jobs">
                <div class="jobs__container">
                    <div class="jobs__text__container">
                        <div class="jobs__text">
                            <span class="jobs__title translatables" data-translate="feature_jobs">Jobs</span>
                            <span class="jobs__description translatables" data-translate="jobs_section_text">Be
                                on the
                                next driver seat in our innovative team team using our tools below.</span>
                            <!-- <a href="#" class='jobs__posting'>See our job postings > </a> -->
                        </div>
                    </div>
                    <div class="jobs__image">
                        <img src="assets/images/section/jobs.png?dummy=8484743" alt="about" />
                    </div>
                </div>
            </div>

            <div class="section__technology">
                <!-- <span class="section__title translatables" data-translate="feature_software">Software Stack</span> -->
                <div class="technology__container">
                </div>
            </div>

            <script>
            (function() {

                function Slideshow(element) {
                    this.el = document.querySelector(element);
                    this.init();
                }

                Slideshow.prototype = {
                    init: function() {
                        this.wrapper = this.el.querySelector(".team__slider-wrapper");
                        this.slides = this.el.querySelectorAll(".team__slide");
                        this.previous = this.el.querySelector(".team__slider-previous");
                        this.next = this.el.querySelector(".team__slider-next");
                        this.index = 0;
                        this.total = this.slides.length;
                        this.timer = null;

                        this.action();
                        this.stopStart();
                    },
                    _slideTo: function(slide) {
                        var currentSlide = this.slides[slide];
                        currentSlide.style.opacity = 1;

                        for (var i = 0; i < this.slides.length; i++) {
                            var slide = this.slides[i];
                            if (slide !== currentSlide) {
                                slide.style.opacity = 0;
                            }
                        }
                    },
                    action: function() {
                        var self = this;
                        self.timer = setInterval(function() {
                            self.index++;
                            if (self.index == self.slides.length) {
                                self.index = 0;
                            }
                            self._slideTo(self.index);

                        }, 10000);
                    },
                    stopStart: function() {
                        // var self = this;
                        // self.el.addEventListener( "mouseover", function() {
                        // 	clearInterval( self.timer );
                        // 	self.timer = null;

                        // }, false);
                        // self.el.addEventListener( "mouseout", function() {
                        // 	self.action();

                        // }, false);
                    }


                };

                document.addEventListener("DOMContentLoaded", function() {

                    var slider = new Slideshow("#main-slider");

                });


            })();
            </script>
        </div>
        <div class="section__footer">
            <div class="footer__container">
                <div class="footer__logo">
                    <img src="assets/images/footer-logo.png" alt="footer logo" />
                </div>
                <div class="footer__navigation">
                    <span class="footer__title translatables" data-translate="navigation">Navigation</span>
                    <ul class="footer__list">
                        <li><a class="text_decoration_none translatables" data-translate="nav_about_us"
                                href="#about">About Us</a></li>
                        <!-- <li><a class="text_decoration_none translatables" data-translate="nav_portfolio" href="#portfolio">Portfolio</a></li> -->
                        <li><a class="text_decoration_none translatables" data-translate="nav_technologies"
                                href="#product_technology">Technologies</a></li>
                        <li><a class="text_decoration_none translatables" data-translate="nav_testimonials"
                                href="#testimonial">Testimonials</a></li>
                        <!-- <li><a class="text_decoration_none translatables" data-translate="nav_jobs" href="#jobs">Jobs</a></li> -->
                        <li><a class="text_decoration_none translatables" data-translate="nav_team"
                                href="#team">Team</a></li>
                    </ul>
                </div>
                <div class="footer__contact">
                    <span class="footer__title translatables" data-translate="contact_us">Contact Us</span>

                    <ul class="footer__list">
                        <li><span><i class="fas fa-map-marker-alt"></i> Malolos City, Philippines 3000</span>
                        </li>

                        <!-- <li class="contact"><span><i class="far fa-envelope"></i> contact@eastgate.tech</span></li> -->
                        <li class="contact">
                            <div class="footer__contact"><img src="assets/images/footer-contact.png" /
                                    alt="footer contact"></div>
                        </li>

                    </ul>

                    <div class="footer__social__media">
                        <a href="https://www.facebook.com/eastgate.tech" class="footer__link"><i
                                class="fab fa-facebook"></i></a>
                        <!-- <a href="https://twitter.com/?lang=en" class="footer__link"><i class="fab fa-twitter"></i></a> -->
                        <a href="https://www.instagram.com/eastgate.tech/?hl=af" class="footer__link"><i
                                class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script>
        $(function() {

            var swiper = new Swiper('.swiper-container', {
                // autoplay: {
                //     delay: 4000,
                //     disableOnInteraction: false
                // },
                loop: true,
                slidesPerView: 'auto',
                initialSlide: 2,
                centeredSlides: true,
                slidesPerGroup: 1,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            });

        function setCookie(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + (value || "") + expires + "; path=/";
        }

        function getCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(";");
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == " ") c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        $(".translatables").each((index, elem) => {
            const translate_key = elem.getAttribute("data-translate");
            const translate_value = jsonData[translate_key];
            $(elem).text(translate_value);
        });

        $(".language_switch").click(function() {
            const language = $(this).data("lang");
            setCookie("language", language, 30);
            window.location.reload();
        });

        $("body").css({
            visibility: "visible"
        });

        });
    </script>

   @include('partial.google-analytics')

</body>

</html>