FROM php:8.1-apache

# Install necessary dependencies
RUN apt-get update && apt-get install -y \
    libzip-dev zip unzip \
    libonig-dev \
    && docker-php-ext-install pdo_mysql zip

# Configure Apache
COPY ./apache2.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite

# Set working directoryexiut
WORKDIR /var/www/html

# Copy project files to container
COPY . .

# Install composer dependencies
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install --no-interaction --optimize-autoloader --no-dev

# Set permissions to allow Laravel to write to storage and bootstrap cache
RUN chown -R www-data:www-data /var/www/html/storage \
    && chmod -R 755 /var/www/html/storage


# Expose port
EXPOSE 80

# Start Apache service
CMD ["apache2-foreground"]
