<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/pricing_plan/real_estate', function () {
    return view('pricing_plan.real_estate');
});
Route::get('/pricing_plan/buy_and_sell', function () {
    return view('pricing_plan.buy_and_sell');
});
